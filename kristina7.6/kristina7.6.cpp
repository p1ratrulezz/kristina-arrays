﻿// Задание 7.6:
// Дана матрица размера M ´ N.
// Поменять местами столбцы, содержащие минимальный и максимальный элементы матрицы.

#include <iostream>
#include <iomanip>
#include "../kristina_library/kristina_library.h"

using namespace std;

int main()
{
	init();

	int M = 0; // Строки матрицы
	int N = 0; // Столбцы матрицы

	cout << "Введите количество строк матрицы M="; cin >> M;
	cout << "Ввдите количество столбцов матрицы N="; cin >> N;


	int** matrix = create_matrix(M, N);

	// Заполняем последовательными данными для теста
	for (int row = 0; row < M; row++)
	{
		for (int col = 0; col < N; col++)
		{
			// matrix[row][col] = row + col;
			cout << "Введите элемент matrix[" << row << "][" << col << "] = "; cin >> matrix[row][col];
		}
	}

	// Показываем матрицу.
	show_matrix(matrix, M, N);

	// Ищем индексы столбцов, которые содержат максимальный и минимальные элементы по всей матрице.
	// По умолчанию считаем, что максимальный и минимальные элементы содержаться в первом столбце (с индексом 0)
	int max_col_id = 0;
	int min_col_id = 0;

	// Запомним значение для минимума и максимума для сравнения.
	int max_value = matrix[0][0];
	int min_value = matrix[0][0];
	for (int row = 0; row < M; row++)
	{
		for (int col = 0; col < N; col = col + 2)
		{
			if (matrix[row][col] > max_value) {
				// Нашли новый максимум, запомнили значение и индекс столбца.
				max_value = matrix[row][col];
				max_col_id = col;
			}
			else if (matrix[row][col] < min_value) {
				// Нашли новый минимум, запомнили значение и индекс столбца.
				min_value = matrix[row][col];
				min_col_id = col;
			}
		}
	}

	cout << "Максимальный элемент " << max_value << " находится в столбце " << max_col_id << endl;
	cout << "Минимальный элемент " << min_value << " находится в столбце " << min_col_id << endl;

	// Меняем местами столбцы max_col_id и min_col_id
	for (int row = 0; row < M; row++)
	{
		swap(matrix[row][max_col_id], matrix[row][min_col_id]);
	}

	// Показываем матрицу снова.
	show_matrix(matrix, M, N);

	// Очищаем выделенную под матрицу памятью.
	matrix_free(matrix, M);

	delay();
	return 0;
}
