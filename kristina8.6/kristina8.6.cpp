﻿// Задание 8.6
// Дана квадратная матрица A порядка M.
// Найти среднее арифметическое элементов каждой ее диагонали, 
// параллельной побочной(начиная с одноэлементной диагонали A[1,1]).

#include <iostream>
#include <iomanip>
#include "../kristina_library/kristina_library.h"

using namespace std;

int main()
{
	init();

	int M = 0; // Строки матрицы
	cout << "Введите порядок матрицы M="; cin >> M;

	int** matrix = create_matrix(M, M);

	// Заполняем последовательными данными для теста
	for (int row = 0; row < M; row++)
	{
		for (int col = 0; col < M; col++)
		{
			// matrix[row][col] = rand() % 1000;
			cout << "Введите элемент matrix[" << row << "][" << col << "] = "; cin >> matrix[row][col];
		}
	}

	// Показываем матрицу.
	show_matrix(matrix, M, M);

	// Все диагонали, начиная от matrix[1][1] - matrix[0][1]
	// matrix[2][2] - matrix[2][1] - matrix[2][0]
	// Количество побочных диагоналей - от 1 до M
	// На первой (нулевой) строке диагонали не строятся, так как выше нет строк
	for (int row = 1; row < M; row++)
	{
		int diag_sum = 0;
		for (int col = row; col >= 0; col--)
		{
			diag_sum = diag_sum + matrix[row][col];
		}

		cout << "Сумма диагонали matrix[" << row << "][" << row << "] равна " << diag_sum << endl;
	}


	// Очищаем выделенную под матрицу памятью.
	matrix_free(matrix, M);

	delay();
	return 0;
}
