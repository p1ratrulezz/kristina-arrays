﻿// Задание 10.6:
// Дан одномерный массив. Сформировать двумерный, отсортированный по спирали
// начиная с левого нижнего угла против часовой стрелки

#include <iostream>
#include <iomanip>
#include <math.h>
#include <time.h>
#include <algorithm>
#include "../kristina_library/kristina_library.h"

#define MIN_ARRAY_SIZE 9

using namespace std;

int main()
{
	init();

	int N = 0; // Количество элементов в матрице.
	cout << "Введите количество элементов матрицы (N> " << MIN_ARRAY_SIZE << ") N="; cin >> N;
	if (N < MIN_ARRAY_SIZE) {
		cout << "Количество элементов должно быть больше " << MIN_ARRAY_SIZE;
		return 1;
	}

	int* arr = create_array(N);
	for (int i = 0; i < N; i++)
	{
		// Заполняем значениями от 0 до 999
		arr[i] = rand() % 1000;
		// Или числами последовательно
		//arr[i] = i; 
		// cout << "Введите элемент массива A[" << i << "]="; cin >> arr[i];
	}

	// Вычисляем оптимальный размер для матрицы, пытаясь сделать из него квадрат.
	// Поскольку для расчета площади используется формула длина X ширина, а у нас нет определенной фиксированной
	// длины или ширины, поэтому мы склоняемся к тому, чтобы наша матрица была максимально квадратной.
	// Например, для 25 - идеальный размер 5 X 5 
	// А вот для 28 - квадрат будет все равно 5, однако размер 5 X 5 
	// не подойдет, поскольку остается целых 3 (28 - 5 * 5) элемента, которые не втиснутся в этот квадрат
	// Поэтому нужно использовать размер 6 X 5

	int median = sqrt(N);
	int cols = floor(N / median);
	int rows = floor(N / cols);

	// То есть количество лишних элементов будет посчитано как 28 - 5 * 5
	int out_of_range_elements = N - (rows * cols);

	// И если такие элементы будут, добавляем еще одну строку, чтобы уместить все элементы одномерного массива.
	if (out_of_range_elements > 0)
	{
		rows++;
	}

	cout << "Массив будет разделен на матрицу размером " << rows << "X" << cols << endl;

	int** matrix = create_matrix(rows, cols);

	// Превращаем массив в матрицу указанного размера
	array_to_matrix(matrix, arr, rows, cols, N);

	// Показываем матрицу.
	cout << "Матрица без сортировки\n";
	show_matrix(matrix, rows, cols);

	// Сортируем одномерный массив стандартной функцией (нам сейчас не до своих сортировок, ибо влом да и зачем...)
	sort(arr, arr+N);
	
	// Теперь имеем на руках уже отсортированный массив. можем доставать из него элементы и класть в матрицу в нужные места.
	int arr_index = 0;
	int col_id = 0;
	int row_id = rows - 1;
	int row_direction = -1; // Вдоль столбцов идем вверх
	int col_direction = 1; // Вдоль строк идем вправо
	int col_left = 0;
	int col_right = cols - 1;
	int row_up = 0;
	int row_bottom = rows - 1;
	bool row_mode = true;
	bool row_filled = false;
	bool col_filled = false;
	int real_N = rows * cols;
	while (arr_index < real_N)
	{
		// Кладем в нужную ячейку следующий по счету элемент из одномерного отсортированного массива.
		if (arr_index < N) {
			matrix[row_id][col_id] = arr[arr_index];
		}
		else
		{
			matrix[row_id][col_id] = 0;
		}

		arr_index++;

		if (row_mode)
		{
			row_id += row_direction;

			// Вышли за границы строк
			if (row_id < row_up || row_id > row_bottom)
			{
				if (row_direction < 0)
				{

					row_id = row_up;
					col_left++;
					col_id = col_left;
				}
				else
				{
					row_id = row_bottom;
					col_right--;
					col_id = col_right;
				}

				row_direction *= -1;
				row_mode = !row_mode;
			}
		}
		else
		{
			// Вычисляем адрес следующего элемента.
			col_id += col_direction;

			// Если вылезли за границы колонок, переходим на следующую строку
			if (col_id < col_left || col_id > col_right)
			{
				
				if (col_direction > 0)
				{
					col_id = col_right;
					row_up++;
					row_id = row_up;
				}
				else
				{
					col_id = col_left;
					row_bottom--;
					row_id = row_bottom;
				}

				col_direction *= -1;
				row_mode = !row_mode;
			}
		}
	}


	// Показываем матрицу.
	cout << "\nМатрица, отсортированная змейкой, начиная с левого нижнего угла:\n";
	show_matrix(matrix, rows, cols);

	// Очищаем выделенную под матрицу памятью.
	matrix_free(matrix, rows);

	// Удаляем одномерный массив.
	delete[] arr;

	delay();
	return 0;
}
