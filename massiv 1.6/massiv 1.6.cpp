﻿// massiv 1.6.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <cmath>
#include <iomanip>
#include "../kristina_library/kristina_library.h"
#define N 10

using namespace std;

void main()
{
	init();

	int arr[N];

	for (int i = 0; i < N; i++)
	{
		cout << "vvedite elemt massiva" << i << ":";
		cin >> arr[i];
	}

	// Вывести массив.
	show_massiv(arr, N);

	// Дан целочисленный массив размера N. Удалить из массива все соседние одинаковые элементы, оставив их первые вхождения.
	int for_del[N];
	int for_del_i = 0;
	for (int i = 0; i < N - 1; i++)
	{
		int curr = arr[i];
		for (int g = i + 1; g < N; g++)
		{
			int next = arr[g];
			if (next == curr)
			{
				for_del[for_del_i] = g;
				for_del_i++;
			}
			else {
				break;
			}
		}
	}

	int new_length = N;
	if (for_del_i > 0)
	{
		for (int d = 0; d < for_del_i; d++)
		{
			for (int i = for_del[d]; i < N - 1; i++)
			{
				arr[i] = arr[i + 1];
			}

			new_length--;
		}
	}

	// Вывести массив.
	cout << endl << endl;
	show_massiv(arr, new_length);

	delay();
}
