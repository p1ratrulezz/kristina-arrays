#pragma once

#include "pch.h"

#define MIN_ARRAY_SIZE 9

using namespace std;

void array_to_matrix(int** matrix, int* arr, int rows, int cols, int N);
int* create_array(int N);
int** create_matrix(int rows, int cols);
void matrix_free(int** matrix, int rows);
void show_matrix(int** matrix, int rows, int cols);
void delay();
void init();
void show_massiv(int* arr, int n);