// kristina_library.cpp : Defines the functions for the static library.
//

#include "pch.h"
#include "framework.h"

#include "kristina_library.h"
#include <iostream>
#include <iomanip>
#include <math.h>
#include <time.h>
#include <algorithm>

using namespace std;

void array_to_matrix(int** matrix, int* arr, int rows, int cols, int N)
{
	int i = 0;
	for (int row = 0; row < rows; row++)
	{
		for (int col = 0; col < cols; col++)
		{
			if (i < N)
			{
				matrix[row][col] = arr[i];
			}
			else
			{
				// ��������� ������ ����������� ��������
				matrix[row][col] = 0;
			}

			//  ������� ����������� �������.
			i++;
		}
	}
}

int* create_array(int N)
{
	// �������� ������ ��� N ��������� ������ ����
	int* arr = new int[N];
	for (int i = 0; i < N; i++)
	{
		// � ������ ���������� 0 ��� �������.
		arr[i] = 0;
	}

	return arr;
}

int** create_matrix(int rows, int cols)
{
	// ������� ������� ������ �� ����������.
	int** matrix = new int* [rows];
	for (int i = 0; i < rows; i++)
	{
		// ��� ������� ��������� �������� ����� ��� �������� cols ��������� ������ ����.
		matrix[i] = create_array(cols);
	}

	return matrix;
}

void matrix_free(int** matrix, int rows)
{
	// ������� ������ ���������� ������, �� ������� ��������� ������� ������.
	for (int i = 0; i < rows; i++)
	{
		delete[] matrix[i];
	}

	// ������� ������, ���������� ��� �������� ����������.
	delete[] matrix;
}

void show_matrix(int** matrix, int rows, int cols)
{
	cout << endl;
	for (int row = 0; row < rows; row++)
	{
		for (int col = 0; col < cols; col++)
		{
			// ����� � ������������� � 4 �������.
			cout << setw(4) << matrix[row][col];
		}

		cout << endl;
	}

	cout << endl;
}

void delay()
{
	cout << endl << "������� ����� ������� ��� ������...";
	getchar();
}

void init()
{
	srand(time(NULL));
	setlocale(LC_ALL, "");
}

void show_massiv(int* arr, int n)
{
	// ������� ������.
	for (int i = 0; i < n; i++)
	{
		cout << setw(3) << arr[i];
	}
}