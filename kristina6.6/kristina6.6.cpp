﻿// kristina6.6.cpp : This file contains the 'main' function. Program execution begins and ends there.
// Задание 6.6:
// Дана матрица размера M ´ N. 
// Для каждого столбца матрицы с четным номером (2, 4, …) 
// найти сумму его элементов. Условный оператор не использовать.

#include <iostream>
#include <iomanip>
#include "../kristina_library/kristina_library.h"

using namespace std;

int main()
{
	init();

	int M = 0; // Строки матрицы
	int N = 0; // Столбцы матрицы

	cout << "Введите количество строк матрицы M="; cin >> M;
	cout << "Ввдите количество столбцов матрицы N="; cin >> N;


	int** matrix = create_matrix(M, N);

	// Заполняем последовательными данными для теста
	for (int row = 0; row < M; row++)
	{
		for (int col = 0; col < N; col++)
		{
			// matrix[row][col] = row + col;
			cout << "Введите элемент matrix[" << row << "][" << col << "] = "; cin >> matrix[row][col];
		}
	}

	// Показываем матрицу.
	show_matrix(matrix, M, N);

	// Считаем суммы для четных столбцов.
	// Начиная от столбца 0 делаем шаг +2
	for (int col = 0; col < N; col = col + 2)
	{
		long sum = 0;
		for (int row = 0; row < M; row++)
		{
			sum = sum + matrix[row][col];
		}

		cout << "Сумма элементов в столбце " << col << " равна " << sum << endl;
	}

    // Очищаем выделенную под матрицу памятью.
	matrix_free(matrix, M);


	delay();
	return 0;
}
